import java.awt.GridLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class TicTacToeGUI extends JFrame {
    private JLabel lblTitle, lblMessage;
    private JButton btnReset, btn1x1, btn1x2, btn1x3, btn2x1, btn2x2, btn2x3, btn3x1, btn3x2, btn3x3;
    private JButton[] btnList;

    public TicTacToeGUI() {
        // Membuat panel utama
        JPanel mainPanel = new JPanel();

        // Menentukan layout dan size frame yang ingin digunakan
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
        mainPanel.setBorder(new EmptyBorder(10, 10, 10, 10)); // memberi border untuk panel utama
        setSize(400, 500);
        setLocationRelativeTo(null); // menaruh window di tengah layar
        

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
        setTitle("Tic Tac Toe by ARK");

        // Membuat panel untuk menampung kotak tic tac toe 3x3
        JPanel panel1 = new JPanel();
        panel1.setMaximumSize(new Dimension(350, 350));
        panel1.setLayout(new GridLayout(3, 3, 2, 2));

        // Membuat label judul
        lblTitle = new JLabel();
        lblTitle.setText("The Game of Tic Tac Toe");
        lblTitle.setFont(new Font("Bauhaus 93", Font.PLAIN, 20));
        lblTitle.setAlignmentX(CENTER_ALIGNMENT);

        // Membuat label message
        lblMessage = new JLabel();
        lblMessage.setText("Let's start!");
        lblMessage.setAlignmentX(CENTER_ALIGNMENT);

        // Membuat button untuk tiap kotak tic tac toe dengan sistem penamaan [baris]x[kolom]
        btn1x1 = new JButton();
        btn1x2 = new JButton();
        btn1x3 = new JButton();
        btn2x1 = new JButton();
        btn2x2 = new JButton();
        btn2x3 = new JButton();
        btn3x1 = new JButton();
        btn3x2 = new JButton();
        btn3x3 = new JButton();
        btnReset = new JButton("Reset");
        btnReset.setAlignmentX(CENTER_ALIGNMENT);

        // Mengatur font text pada button tic tac toe
        btn1x1.setFont(new Font("Sans Serif", Font.BOLD, 40));
        btn1x2.setFont(new Font("Sans Serif", Font.BOLD, 40));
        btn1x3.setFont(new Font("Sans Serif", Font.BOLD, 40));
        btn2x1.setFont(new Font("Sans Serif", Font.BOLD, 40));
        btn2x2.setFont(new Font("Sans Serif", Font.BOLD, 40));
        btn2x3.setFont(new Font("Sans Serif", Font.BOLD, 40));
        btn3x1.setFont(new Font("Sans Serif", Font.BOLD, 40));
        btn3x2.setFont(new Font("Sans Serif", Font.BOLD, 40));
        btn3x3.setFont(new Font("Sans Serif", Font.BOLD, 40));

        // Memasukkan semua button yang ada di dalam kotak tic tac toe ke array btnList
        btnList = new JButton[] {btn1x1, btn1x2, btn1x3, btn2x1, btn2x2, btn2x3, btn3x1, btn3x2, btn3x3};

        // Memasukkan button ke panel kotak 3x3
        panel1.add(btn1x1);
        panel1.add(btn1x2);
        panel1.add(btn1x3);
        panel1.add(btn2x1);
        panel1.add(btn2x2);
        panel1.add(btn2x3);
        panel1.add(btn3x1);
        panel1.add(btn3x2);
        panel1.add(btn3x3);

        // Memasukkan panel-panel ke dalam panel utama
        mainPanel.add(lblTitle);
        mainPanel.add(panel1);
        mainPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        mainPanel.add(lblMessage);
        mainPanel.add(Box.createRigidArea(new Dimension(0, 5)));
        mainPanel.add(btnReset);

        // Memasukkan panel utama ke frame
        add(mainPanel);

        setVisible(true);

        btn1x1.addActionListener(e -> {
            // Cek apakah game sudah selesai
            if (TicTacToe.getGameOver()) {
                JOptionPane.showMessageDialog(this,
                "The game is over. Please click the reset button.", "Error", JOptionPane.ERROR_MESSAGE);
            }

            else {
                // Mengisi box tic tac toe
                fillBox(0);

                // Cek kondisi menang
                lblMessage.setText(TicTacToe.checkWin());
            }
        });

        btn1x2.addActionListener(e -> {
            // Cek apakah game sudah selesai
            if (TicTacToe.getGameOver()) {
                JOptionPane.showMessageDialog(this,
                "The game is over. Please click the reset button.", "Error", JOptionPane.ERROR_MESSAGE);
            }

            else {
                // Mengisi box tic tac toe
                fillBox(1);

                // Cek kondisi menang
                lblMessage.setText(TicTacToe.checkWin());
            }
        });

        btn1x3.addActionListener(e -> {
            // Cek apakah game sudah selesai
            if (TicTacToe.getGameOver()) {
                JOptionPane.showMessageDialog(this,
                "The game is over. Please click the reset button.", "Error", JOptionPane.ERROR_MESSAGE);
            }

            else {
                // Mengisi box tic tac toe
                fillBox(2);

                // Cek kondisi menang
                lblMessage.setText(TicTacToe.checkWin());
            }
        });

        btn2x1.addActionListener(e -> {
            // Cek apakah game sudah selesai
            if (TicTacToe.getGameOver()) {
                JOptionPane.showMessageDialog(this,
                "The game is over. Please click the reset button.", "Error", JOptionPane.ERROR_MESSAGE);
            }

            else {
                // Mengisi box tic tac toe
                fillBox(3);

                // Cek kondisi menang
                lblMessage.setText(TicTacToe.checkWin());
            }
        });

        btn2x2.addActionListener(e -> {
            // Cek apakah game sudah selesai
            if (TicTacToe.getGameOver()) {
                JOptionPane.showMessageDialog(this,
                "The game is over. Please click the reset button.", "Error", JOptionPane.ERROR_MESSAGE);
            }

            else {
                // Mengisi box tic tac toe
                fillBox(4);

                // Cek kondisi menang
                lblMessage.setText(TicTacToe.checkWin());
            }
        });

        btn2x3.addActionListener(e -> {
            // Cek apakah game sudah selesai
            if (TicTacToe.getGameOver()) {
                JOptionPane.showMessageDialog(this,
                "The game is over. Please click the reset button.", "Error", JOptionPane.ERROR_MESSAGE);
            }

            else {
                // Mengisi box tic tac toe
                fillBox(5);

                // Cek kondisi menang
                lblMessage.setText(TicTacToe.checkWin());
            }
        });

        btn3x1.addActionListener(e -> {
            // Cek apakah game sudah selesai
            if (TicTacToe.getGameOver()) {
                JOptionPane.showMessageDialog(this,
                "The game is over. Please click the reset button.", "Error", JOptionPane.ERROR_MESSAGE);
            }

            else {
                // Mengisi box tic tac toe
                fillBox(6);

                // Cek kondisi menang
                lblMessage.setText(TicTacToe.checkWin());
            }
        });

        btn3x2.addActionListener(e -> {
            // Cek apakah game sudah selesai
            if (TicTacToe.getGameOver()) {
                JOptionPane.showMessageDialog(this,
                "The game is over. Please click the reset button.", "Error", JOptionPane.ERROR_MESSAGE);
            }

            else {
                // Mengisi box tic tac toe
                fillBox(7);

                // Cek kondisi menang
                lblMessage.setText(TicTacToe.checkWin());
            }
        });

        btn3x3.addActionListener(e -> {
            // Cek apakah game sudah selesai
            if (TicTacToe.getGameOver()) {
                JOptionPane.showMessageDialog(this,
                "The game is over. Please click the reset button.", "Error", JOptionPane.ERROR_MESSAGE);
            }

            else {
                // Mengisi box tic tac toe
                fillBox(8);

                // Cek kondisi menang
                lblMessage.setText(TicTacToe.checkWin());
            }
        });

        btnReset.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                // Me-reset game tic tac toe
                TicTacToe.reset();
                resetGUI();
            }
        });
    }

    // Mengisi box tic tac toe dibalik layar dan di depan layar
    public void fillBox(int index) {

        // Mengisi box untuk giliran X (turn ganjil)
        if (TicTacToe.getTurn() % 2 != 0) {
            // Mengisi box dibalik layar (backend processing)
            TicTacToe.getMarkedX().add(index);
            TicTacToe.getAllBox().add(index);

            // Mengisi box di depan layar (frontend processing)
            btnList[index].setText("X");
        }

        // Mengisi box untuk giliran O (turn genap)
        else {
            // Mengisi box dibalik layar (backend processing)
            TicTacToe.getMarkedO().add(index);
            TicTacToe.getAllBox().add(index);

            // Mengisi box di depan layar (frontend processing)
            btnList[index].setText("O");
        }

        // Increment turn
        TicTacToe.setTurn(TicTacToe.getTurn() + 1);
    }

    // Menghapus isi box tic tac toe dan me-reset label message
    public void resetGUI() {
        // Mengosongkan box tic tac toe
        btn1x1.setText("");
        btn1x2.setText("");
        btn1x3.setText("");
        btn2x1.setText("");
        btn2x2.setText("");
        btn2x3.setText("");
        btn3x1.setText("");
        btn3x2.setText("");
        btn3x3.setText("");

        // Mengembalikan text di label message kembali ke awal
        lblMessage.setText("Let's start!");
    }
}