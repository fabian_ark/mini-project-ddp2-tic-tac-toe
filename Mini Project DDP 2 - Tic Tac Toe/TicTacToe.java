import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TicTacToe {
    private static ArrayList<List<Integer>> winCondition = new ArrayList<List<Integer>>();
    private static Set<Integer> markedX = new HashSet<Integer>();
    private static Set<Integer> markedO = new HashSet<Integer>();
    private static Set<Integer> allBox = new HashSet<Integer>();
    private static int turn = 1;
    private static boolean gameOver = false;

    public static void main(String[] args) {
        fillWinCondition(); // mengisi winCondition
        new TicTacToeGUI(); // init instance TicTacToeGUI yang baru
    }

    // Mengisi kondisi atau posisi kemenangan ke arraylist winCondition
    public static void fillWinCondition() {
        // Horizontal win
        winCondition.add(Arrays.asList(0,1,2));
        winCondition.add(Arrays.asList(3,4,5));
        winCondition.add(Arrays.asList(6,7,8));

        // Vertical win
        winCondition.add(Arrays.asList(0,3,6));
        winCondition.add(Arrays.asList(1,4,7));
        winCondition.add(Arrays.asList(2,5,8));

        // Diagonal win
        winCondition.add(Arrays.asList(0,4,8));
        winCondition.add(Arrays.asList(2,4,6));
    }

    // Mengecek apakah sudah ada yang menang atau belum sekaligus mengecek kondisi seri
    public static String checkWin() {
        // Mengecek apakah X menang atau tidak
        if (markedX.containsAll(winCondition.get(0)) || markedX.containsAll(winCondition.get(1)) ||
            markedX.containsAll(winCondition.get(2)) || markedX.containsAll(winCondition.get(3)) ||
            markedX.containsAll(winCondition.get(4)) || markedX.containsAll(winCondition.get(5)) ||
            markedX.containsAll(winCondition.get(6)) || markedX.containsAll(winCondition.get(7))) {
            gameOver = true;
            return "X wins! Please click the reset button.";
        }
        // Mengecek apakah O menang atau tidak
        else if (markedO.containsAll(winCondition.get(0)) || markedO.containsAll(winCondition.get(1)) ||
                 markedO.containsAll(winCondition.get(2)) || markedO.containsAll(winCondition.get(3)) ||
                 markedO.containsAll(winCondition.get(4)) || markedO.containsAll(winCondition.get(5)) ||
                 markedO.containsAll(winCondition.get(6)) || markedO.containsAll(winCondition.get(7))) {
            gameOver = true;
            return "O wins! Please click the reset button.";
        }
        // Mengecek apakah game dalam kondisi seri (kotak sudah penuh dan belum ada yang menang)
        else if (allBox.containsAll(Arrays.asList(0,1,2,3,4,5,6,7,8))) {
            gameOver = true;
            return "Draw! Please click the reset button.";
        }
        // Lanjut permainan jika belum terjadi kemenangan atau seri
        else
            return "Let's start!";
    }

    // Me-reset game (mengosongkan penanda X dan O, mengembalikan nilai awal turn dan gameover)
    public static void reset() {
        markedX.clear();
        markedO.clear();
        allBox.clear();
        turn = 1;
        gameOver = false;
    }

    // Getter and setter method
    public static Set<Integer> getMarkedX() {
        return markedX;
    }

    public static Set<Integer> getMarkedO() {
        return markedO;
    }

    public static Set<Integer> getAllBox() {
        return allBox;
    }

    public static int getTurn() {
        return turn;
    }

    public static void setTurn(int turn) {
        TicTacToe.turn = turn;
    }

    public static boolean getGameOver() {
        return gameOver;
    }
}